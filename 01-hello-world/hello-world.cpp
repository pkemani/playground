#include <iostream>
using namespace std;
/**
 *  First C++ program
 *  Hello world program
 */
int main()
{
    cout << "Hello World!\n";
    cout << "Hello World!"
         << endl            // end line similar to new line \n
         << "My name is Praveen \n";
    return 0;
}